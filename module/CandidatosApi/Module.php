<?php
/**
 * Módulo da API de Candidatos
 */

namespace CandidatosApi;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'candidatosapi_cadastro_form_hydrator' => function($services){
                    return new \DoctrineModule\Stdlib\Hydrator\DoctrineObject($services->get('doctrine.entitymanager.orm_default'));
                },
                'candidatosapi_candidato_mapper' => function($services) {
                    return new Mapper\Candidato($services->get('doctrine.entitymanager.orm_default'));
                },
                'candidatosapi_conhecimento_mapper' => function($services) {
                    return new Mapper\Conhecimento($services->get('doctrine.entitymanager.orm_default'));
                },
                'candidatosapi_cadastro_form' => function($services) {
                    $form = new Form\CadastroForm($services->get('vagasapi_vaga_mapper'), $services->get('candidatosapi_conhecimento_mapper'));
                    $form->setHydrator($services->get('candidatosapi_cadastro_form_hydrator'));
                    return $form;
                }
            ),
        );
    }

    public function getControllerConfig()
    {
        return array(
            'factories' => array(
                'CandidatosApi\Controller\CandidatoServer' => function($controllers) {
                    return new Controller\CandidatoServer(
                        $controllers->getServiceLocator()->get('candidatosapi_candidato_mapper'),
                        $controllers->getServiceLocator()->get('candidatosapi_conhecimento_mapper'),
                        $controllers->getServiceLocator()->get('candidatosapi_cadastro_form')
                    );
                }
            )
        );
    }
}
