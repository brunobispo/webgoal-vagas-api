<?php

return array(
    'router' => array(
        'routes' => array(
            'candidatos-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/candidatos',
                    'defaults' => array(
                        'controller' => 'CandidatosApi\Controller\CandidatoServer',
                    )
                )
            )
        )
    ),
    'doctrine' => array(
        'driver' => array(
            'ORM_Driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/CandidatosApi/Model')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'CandidatosApi\Model' => 'ORM_Driver'
                )
            )
        )
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);
