<?php
/**
 * Serviço de candidatos a ser consumido
 */

namespace CandidatosApi\Controller;


use ApiBase\Controller\AbstractJsonRpcServer;
use CandidatosApi\Form\CadastroForm;
use Sds\Zf2ExtensionsModule\Controller\AbstractJsonpController;
use Zend\Http\Response;
use CandidatosApi\Mapper\CandidatoInterface as CandidatoMapper;
use CandidatosApi\Mapper\ConhecimentoInterface as ConhecimentoMapper;

class CandidatoServer extends AbstractJsonRpcServer
{
    protected $mapper;
    protected $conhecimentosMapper;
    protected $cadastroForm;

    public function registerRpcMethods()
    {
        return array('create', 'findAllConhecimentos');
    }

    function __construct(CandidatoMapper $mapper, ConhecimentoMapper $conhecimentos, CadastroForm $cadastroForm)
    {
        $this->mapper = $mapper;
        $this->cadastroForm = $cadastroForm;
        $this->conhecimentosMapper = $conhecimentos;
    }

    /**
     * @param array $data
     * @return array|mixed
     */
    public function create(array $data)
    {
        $form = $this->cadastroForm;
        $form->setData($data);

        if (!$form->isValid()) {
            return array('errorMessages' => $form->getMessages());
        } else {
            $entity = $form->getData();
            return $this->prepareModels($this->mapper->create($entity));
        }
    }

    /**
     * @param array $criteria
     * @return array
     */
    public function searchBy(array $criteria)
    {
        $entities = $this->mapper->searchBy($criteria);
        return $this->prepareModels($entities);
    }

    public function findAllConhecimentos()
    {
        return $this->prepareModels($this->conhecimentosMapper->findAll());
    }



}