<?php
/**
 * Formulário de cadastro de candidatos
 */

namespace CandidatosApi\Form;


use CandidatosApi\Model\Candidato;
use VagasApi\Mapper\VagaInterface as VagaMapper;
use CandidatosApi\Mapper\ConhecimentoInterface as ConhecimentoMapper;
use Zend\Form\Form;

class CadastroForm extends Form
{
    public function __construct(VagaMapper $vagasMapper, ConhecimentoMapper $conhecimentosMapper, $name = null, $options = array())
    {
        parent::__construct($name, $options);

        $model = new Candidato();
        $model->setVagasMapper($vagasMapper);
        $this->setObject($model);

        $this->add(array(
            'name' => 'nome',
        ));

        $this->add(array(
            'name' => 'nascimento',
        ));

        $this->add(array(
            'name' => 'ondeEstudou',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'ondeTrabalhou',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'experiencia',
            'type' => 'textarea',
        ));

        $this->add(array(
            'name' => 'pretensaoSalarial',
            'type' => 'text',
        ));

        /*$this->add(array(
            'name' => 'conhecimentos',
            'type' => 'CandidatosApi\Form\Element\ConhecimentoMultiCheckbox',
            'options' => array(
                'mapper' => $conhecimentosMapper
            ),
        ));*/

        $this->add(array(
            'name' => 'vaga',
            'type' => 'CandidatosApi\Form\Element\VagaSelect',
            'options' => array(
                'mapper' => $vagasMapper
            ),
        ));
    }



}