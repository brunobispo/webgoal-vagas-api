<?php
/**
 * Elemento select de vagas
 */

namespace CandidatosApi\Form\Element;


use Zend\Form\Element\Select;

class VagaSelect extends Select
{
    protected $disableInArrayValidator = true;

    protected $mapper;

    public function setOptions($options)
    {
        $result = parent::setOptions($options);

        if (isset($options['mapper'])) {
            $this->setMapper($options['mapper']);
        }

        return $result;
    }

    public function setMapper($mapper)
    {
        $this->mapper = $mapper;
    }

    public function getMapper()
    {
        return $this->mapper;
    }

    public function getValueOptions()
    {
        $entities = $this->getMapper()->findAll();
        $map = array();
        foreach ($entities as $entity) {
            $map[$entity->getId()] = $entity->getTitulo();
        }

        return $map;
    }


}