<?php

namespace CandidatosApi\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="conhecimento")
 */
class Conhecimento
{
     /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     **/
    protected $id;

	/**
     * @ORM\Column(type="string")
     * @var string
     */
	protected $nome;

     public function getId()
     {
          return $this->id;
     }

     public function setId($id)
     {
          $this->id = $id;
     }

     public function getNome()
     {
          return $this->nome;
     }

     public function setNome($id)
     {
          $this->nome = $id;
     }

     public function toArray()
    {
        return get_object_vars($this);
    }
}