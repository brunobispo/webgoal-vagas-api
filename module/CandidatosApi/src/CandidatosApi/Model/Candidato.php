<?php

/**
 * Entidade Candidato
 */

namespace CandidatosApi\Model;

use Doctrine\ORM\Mapping as ORM;
use VagasApi\Model\Vaga;
use VagasApi\Mapper\VagaInterface as VagaMapper;
use Zend\InputFilter\Factory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="candidato")
 */
class Candidato implements InputFilterAwareInterface
{
    /**
     * @var InputFilterInterface
     */
    protected $inputFilter;

    /**
     * Usado para validar as vagas existentes
     *
     * @var VagaMapper;
     */
    protected $vagasMapper;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     **/
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $nome;

    /**
     * @ORM\Column(type="date")
     * @var string
     */
    protected $nascimento;

    /**
     * @ORM\ManyToOne(targetEntity="VagasApi\Model\Vaga")
     * @var string
     */
    protected $vaga;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    protected $ondeEstudou;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    protected $ondeTrabalhou;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    protected $experiencia;

    /**
     * @ORM\Column(type="decimal")
     * @var string
     */
    protected $pretensaoSalarial;

    /**
     * @ORM\ManyToMany(targetEntity="Conhecimento", inversedBy="candidatos")
     * @var Conhecimento[]
     */
    protected $conhecimentos;

    public function __constructor()
    {
        $this->conhecimentos = ArrayCollection();
    }

    /**
     * @param string $experiencia
     */
    public function setExperiencia($experiencia)
    {
        $this->experiencia = $experiencia;
    }

    /**
     * @return string
     */
    public function getExperiencia()
    {
        return $this->experiencia;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $nascimento
     */
    public function setNascimento(\DateTime $nascimento)
    {
        $this->nascimento = $nascimento;
    }

    /**
     * @return string
     */
    public function getNascimento()
    {
        return $this->nascimento;
    }

    /**
     * @param string $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param string $ondeEstudou
     */
    public function setOndeEstudou($ondeEstudou)
    {
        $this->ondeEstudou = $ondeEstudou;
    }

    /**
     * @return string
     */
    public function getOndeEstudou()
    {
        return $this->ondeEstudou;
    }

    /**
     * @param string $ondeTrabalhou
     */
    public function setOndeTrabalhou($ondeTrabalhou)
    {
        $this->ondeTrabalhou = $ondeTrabalhou;
    }

    /**
     * @return string
     */
    public function getOndeTrabalhou()
    {
        return $this->ondeTrabalhou;
    }

    /**
     * @param string $pretensaoSalarial
     */
    public function setPretensaoSalarial($pretensaoSalarial)
    {
        $this->pretensaoSalarial = $pretensaoSalarial;
    }

    /**
     * @return string
     */
    public function getPretensaoSalarial()
    {
        return $this->pretensaoSalarial;
    }

    /**
     * @param string $vaga
     */
    public function setVaga(Vaga $vaga)
    {
        $this->vaga = $vaga;
    }

    /**
     * @return string
     */
    public function getVaga()
    {
        return $this->vaga;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

    /**
     * Set input filter
     *
     * @param  InputFilterInterface $inputFilter
     * @return InputFilterAwareInterface
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception('Não implementado');
    }



    /**
     * Retrieve input filter
     *
     * @return InputFilterInterface
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $factory = new Factory();
            $inputFilter = new InputFilter();

            $inputFilter->add($factory->createInput(array(
                'name' => 'nome',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'message' => 'É obrigatório informar seu nome'
                        ),
                    ),
                    array(
                        'name' => 'Alpha',
                        'options' => array(
                            'message' => 'Só é permitido informar letras',
                            'allowWhiteSpace' => true
                        )
                    ),
                ),
                'filters' => array(
                    array('name' => 'StringTrim')
                )
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'nascimento',
                'required' => true,
                'filters' => array(
                    array('name' => 'Zend\Filter\StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'message' => 'É obrigatório informar seu nome'
                        )
                    ),
                    array(
                        'name' => 'date',
                        'options' => array(
                            'message' => 'Você precisa informar uma data válida'
                        )
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'ondeEstudou',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'message' => 'É obrigatório informar onde estudou'
                        ),
                    ),
                ),
                'filters' => array(
                    array('name' => 'StringTrim'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'ondeTrabalhou',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'message' => 'É obrigatório informar onde trabalhou'
                        ),
                    ),
                ),
                'filters' => array(
                    array('name' => 'StringTrim'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'experiencia',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'message' => 'É obrigatório informar seus conhecimentos'
                        ),
                    ),
                ),
                'filters' => array(
                    array('name' => 'StringTrim'),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'pretensaoSalarial',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'message' => 'É obrigatório informar sua pretensão salarial'
                        ),
                        'break_chain_on_failure' => true,
                    ),
                    array(
                        'name' => 'Float',
                        'options' => array(
                            'message' => 'Esse não é um valor numérico válido'
                        )
                    ),
                ),
                'filters' => array(
                    array(
                        'name' => 'PregReplace',
                        'options' => array(
                            'pattern' => '/\,/',
                            'replacement' => '.'
                        )
                    ),
                    array('name' => 'StringTrim'),
                    array('name' => 'Null')
                ),
            )));

            $vagasIds = array_map(function($entity){ return $entity->getId(); }, $this->getVagasMapper()->findAll());

            $inputFilter->add($factory->createInput(array(
                'name' => 'vaga',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'message' => 'É obrigatório escolher uma vaga'
                        ),
                        'break_chain_on_failure' => true,
                    ),
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => $vagasIds,
                            'message' => 'Você precisa informar uma vaga existente'
                        ),
                    ),
                ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * @param VagaMapper $vagasMapper
     */
    public function setVagasMapper($vagasMapper)
    {
        $this->vagasMapper = $vagasMapper;
    }

    /**
     * @return VagaMapper
     */
    public function getVagasMapper()
    {
        return $this->vagasMapper;
    }


}