<?php

/**
 * Persiste e obtêm models Candidatos
 */

namespace CandidatosApi\Mapper;

use ApiBase\Mapper\AbstractDoctrineCrudMapper;

class Candidato extends AbstractDoctrineCrudMapper
    implements CandidatoInterface
{
    protected function getEntityClass()
    {
        return 'CandidatosApi\Model\Candidato';
    }

}