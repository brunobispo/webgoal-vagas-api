<?php
/**
 * Interface do mapper do model Candidato
 */

namespace CandidatosApi\Mapper;

interface ConhecimentoInterface
{
    public function findAll();
}