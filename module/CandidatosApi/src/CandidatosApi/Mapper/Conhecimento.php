<?php

/**
 * Persiste e obtêm models Conhecimentos
 */

namespace CandidatosApi\Mapper;

use ApiBase\Mapper\AbstractDoctrineCrudMapper;

class Conhecimento extends AbstractDoctrineCrudMapper
    implements ConhecimentoInterface
{
    protected function getEntityClass()
    {
        return 'CandidatosApi\Model\Conhecimento';
    }

}