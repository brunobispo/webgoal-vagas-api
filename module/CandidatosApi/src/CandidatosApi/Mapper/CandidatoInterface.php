<?php
/**
 * Interface do mapper do model Candidato
 */

namespace CandidatosApi\Mapper;

interface CandidatoInterface
{
    public function create($candidato);
}