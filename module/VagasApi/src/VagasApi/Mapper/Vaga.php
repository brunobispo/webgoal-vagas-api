<?php

/**
 * Persiste e obtêm models Vaga
 */

namespace VagasApi\Mapper;

use ApiBase\Mapper\AbstractDoctrineCrudMapper;

class Vaga extends AbstractDoctrineCrudMapper
    implements VagaInterface
{
    protected function getEntityClass()
    {
        return 'VagasApi\Model\Vaga';
    }

    public function searchBy(array $criteria)
    {
        $filters = array('titulo_descricao', 'descricao', 'titulo', 'beneficios', 'requisitos');

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('e');
        $qb->from($this->getEntityClass(), 'e');

        foreach ($criteria as $key => $value) {
            if (!in_array($key, $filters)) {
                throw new \Exception('Chave pesquisada não existe');
            }

            switch ($key) {
                case "titulo_descricao":
                    $qb->andWhere("e.titulo LIKE :titulo_descricao OR e.descricao LIKE :titulo_descricao")
                        ->setParameter('titulo_descricao', "%$value%");
                break;
                default:
                    $qb->andWhere("e.$key LIKE :$key")->setParameter($key, "%$value%");
            }
        }

        return $qb->getQuery()->execute();
    }


}