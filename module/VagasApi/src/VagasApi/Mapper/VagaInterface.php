<?php
/**
 * Interface do mapper do model Vaga
 */

namespace VagasApi\Mapper;


interface VagaInterface
{
    public function findAll();
    public function searchBy(array $criteria);
}