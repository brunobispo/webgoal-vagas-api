<?php

/**
 * Entidade Vaga
 */

namespace VagasApi\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="vaga")
 */
class Vaga
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     **/
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $titulo;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    protected $descricao;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    protected $requisitos;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    protected $beneficios;

    /**
     * @param string $beneficios
     */
    public function setBeneficios($beneficios)
    {
        $this->beneficios = $beneficios;
    }

    /**
     * @return string
     */
    public function getBeneficios()
    {
        return $this->beneficios;
    }

    /**
     * @param string $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return string
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $requisitos
     */
    public function setRequisitos($requisitos)
    {
        $this->requisitos = $requisitos;
    }

    /**
     * @return string
     */
    public function getRequisitos()
    {
        return $this->requisitos;
    }

    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

}