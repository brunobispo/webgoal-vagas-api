<?php
/**
 * Serviço de vagas a ser consumido
 */

namespace VagasApi\Controller;


use ApiBase\Controller\AbstractJsonRpcServer;
use Sds\Zf2ExtensionsModule\Controller\AbstractJsonpController;
use Zend\Http\Response;
use VagasApi\Mapper\Vaga as VagaMapper;

class VagaServer extends AbstractJsonRpcServer
{
    protected $mapper;

    public function registerRpcMethods()
    {
        return array('findAll', 'searchBy');
    }

    function __construct(VagaMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->prepareModels($this->mapper->findAll());
    }

    /**
     * @param array $criteria
     * @return array
     */
    public function searchBy(array $criteria)
    {
        $entities = $this->mapper->searchBy($criteria);
        return $this->prepareModels($entities);
    }



}