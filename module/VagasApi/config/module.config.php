<?php

return array(
    'router' => array(
        'routes' => array(
            'vagas-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/vagas',
                    'defaults' => array(
                        'controller' => 'VagasApi\Controller\VagaServer',
                    )
                )
            )
        )
    ),
    'doctrine' => array(
        'driver' => array(
            'ORM_Driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/VagasApi/Model')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'VagasApi\Model' => 'ORM_Driver'
                )
            )
        )
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);
