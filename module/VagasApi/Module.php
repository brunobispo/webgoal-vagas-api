<?php
/**
 * Módulo da API de Vagas
 */

namespace VagasApi;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'vagasapi_vaga_mapper' => function($services) {
                    return new Mapper\Vaga($services->get('doctrine.entitymanager.orm_default'));
                },
            ),
        );
    }

    public function getControllerConfig()
    {
        return array(
            'factories' => array(
                'VagasApi\Controller\VagaServer' => function($controllers) {
                    return new Controller\VagaServer($controllers->getServiceLocator()->get('vagasapi_vaga_mapper'));
                }
            )
        );
    }
}
