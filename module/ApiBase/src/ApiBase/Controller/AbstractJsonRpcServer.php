<?php

namespace ApiBase\Controller;


use Sds\Zf2ExtensionsModule\Controller\AbstractJsonRpcController;

abstract class AbstractJsonRpcServer extends AbstractJsonRpcController
{
    /**
     * Converte os models em arrays para futura renderização em JSON
     *
     * @param $models
     * @return array|mixed
     */
    protected function prepareModels($models)
    {
        if (!is_array($models)) {
            $models = array($models);
            $uniq = true;
        } else {
            $uniq = false;
        }

        foreach ($models as $key => $model) {
            if (method_exists($model, 'toArray')) {
                $models[$key] = $model->toArray();
            }
        }

        return !$uniq ? $models : current($models);

    }
}