<?php

namespace ApiBase\Mapper;


use Doctrine\ORM\EntityManager;

abstract class AbstractDoctrineCrudMapper
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    abstract protected function getEntityClass();

    public function findAll()
    {
        return $this->getRepository()->findAll();
    }

    public function find($id)
    {
        return $this->getRepository()->find($id);
    }

    public function create($object)
    {
        $this->persist($object);
        return $object;
    }

    public function update($object)
    {
        $this->persist($object);
        return $object;
    }

    public function delete($object)
    {
        $this->entityManager->remove($object);
        $this->entityManager->flush();
    }

    /**
     * @param \Doctrine\ORM\EntityManager $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    protected function getRepository()
    {
        return $this->entityManager->getRepository($this->getEntityClass());
    }

    protected function persist($object)
    {
        $this->entityManager->persist($object);
        $this->entityManager->flush();
    }

}